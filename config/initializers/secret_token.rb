# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Task::Application.config.secret_key_base = 'f9eac5dca468bc133d4f104431e9de297a0f4f852eada83bb2b475538a324e4e666c5665b2b497aa20b1fcbe5119f3034a5fd01ae011c7a2b84082d7d511eac2'
