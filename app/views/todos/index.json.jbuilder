json.array!(@todos) do |todo|
  json.extract! todo, :name, :due, :client, :description
  json.url todo_url(todo, format: :json)
end
